/*
  Project GitLab Link
    - https://gitlab.com/msaidbilgehan/home-pet-project-remotexy-bluetooth

  RemoteXY App Link
    - http://remotexy.com/en/editor/28c5b2f03887084892527ae27f2e36a0/

  Water Sensor:
    - https://create.arduino.cc/projecthub/omer-beden/basic-water-sensor-190c7a

  Bluetooth Sensor;
    - https://remotexy.com/en/examples/firstproject/

  Weight Sensor
    - https://steemit.com/utopian-io/@drencolha/making-a-weight-scale-with-the-hx711-module-hx711-arduino-library-and-a-load-cell-tutorial
    - https://github.com/bogde/HX711
    - https://create.arduino.cc/projecthub/electropeak/digital-force-gauge-weight-scale-w-loadcell-arduino-7a7fd5

*/

/*
   -- Pet Home Bluetooth App --

   This source code of graphical user interface
   has been generated automatically by RemoteXY editor.
   To compile this code using RemoteXY library 2.4.3 or later version
   download by link http://remotexy.com/en/library/
   To connect using RemoteXY mobile app by link http://remotexy.com/en/download/
     - for ANDROID 4.5.1 or later version;
     - for iOS 1.4.1 or later version;

   This source code is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.
*/


// ====================
//  DEVELOPER VARIABLES
// ====================

// For testing without sensors, simulate them like working
// This will creates random values for sensor readings
// Also there are ranges for minimum and maximum
// values of random sensor value creation
// !!! IMPORTANT !!! While Simulation is active, weight module wil not be initilized
// It stucks at setup while there is no phisical sensor
#define IS_SENSOR_SIMULATON_ACTIVE false

#if IS_SENSOR_SIMULATON_ACTIVE
  #define RANDOM_MIN 0
  #define RANDOM_MAX 100
#endif

#define IS_VERBOSE true
#define IS_WEIGHT_CALIBRATIN_ACTIVATED  false              // To calibrate weight sensor

#if IS_WEIGHT_CALIBRATIN_ACTIVATED
  // Serial Activated (User Input Variable)
  #define MORE_25_BUTTON                'w'
  #define LESS_25_BUTTON                's'
  #define MORE_250_BUTTON               'e'
  #define LESS_250_BUTTON               'd'
  #define TARE_BUTTON                   't'
  #define DEFAULT_BUTTON                'd'
  #define EXIT_BUTTON                   'q'
  #define CALIBRATION_FACTOR_CHANGE     25
#endif


//////////////////////////////////////////////
//        RemoteXY include library          //
//////////////////////////////////////////////

// RemoteXY select connection mode and include library
#define REMOTEXY_MODE__SOFTSERIAL
#include <SoftwareSerial.h>

#include <RemoteXY.h>

// RemoteXY connection settings
#define REMOTEXY_SERIAL_RX 2
#define REMOTEXY_SERIAL_TX 3
#define REMOTEXY_SERIAL_SPEED 9600


// RemoteXY configurate
#pragma pack(push, 1)
uint8_t RemoteXY_CONF[] =
  { 255,1,0,10,0,49,0,10,147,1,
  66,2,34,43,21,27,78,76,66,2,
  8,43,21,27,177,166,1,0,25,75,
  12,12,2,31,88,0,68,50,2,2,
  59,34,145,78,163,70,111,111,100,0,
  87,97,116,101,114,0 };

// this structure defines all the variables and events of your control interface
struct {

    // input variables
  uint8_t connection_test_button; // =1 if button pressed, else =0

    // output variables
  int8_t food_level; // =0..100 level position
  int8_t water_level; // =0..100 level position
  float level_graph_var1;
  float level_graph_var2;

    // other variable
  uint8_t connect_flag;  // =1 if wire connected, else =0

} RemoteXY;
#pragma pack(pop)


// ====================
// IMPORTS
// ====================
#include <HX711.h>


// ====================
// PIN CONFIGURATIONS
// ====================
#define SENSOR_PIN_WATER                    A3  // Attach Water sensor to Arduino Analog pin 3 - A3
#define SENSOR_PIN_WEIGHT_FIRST_PIN         A1  // Attach Weight sensor to Arduino Analog pin 1 - A1
#define SENSOR_PIN_WEIGHT_SECOND_PIN        A0  // Attach Weight sensor to Arduino Analog pin 0 - A0
#define PIN_CONNECTION_TEST_BUTTON          12  // Attach LED to Arduino Digital 12 Pin

// Global (Shared) Variables
int food_sensor_average                   = 5;

#if IS_WEIGHT_CALIBRATIN_ACTIVATED
  // Serial Activated (User Input Variable)
  char user_input                       = "";
#endif

// Initializes HX711 library functions
// Optimum walue for 1kg to get -100 value is -19750
HX711 scale;
int calibration_factor                    = -19750; // Defines calibration factor we'll use for calibrating.


// ====================
//  BUILT-IN FUNCTIONS
// ====================
void setup(){
  // REMOTEXY INITIALIZATION
  RemoteXY_Init ();

  // LED INITIALIZATION
  pinMode (PIN_CONNECTION_TEST_BUTTON, OUTPUT);

  #if IS_SENSOR_SIMULATON_ACTIVE
  ;
  #else
    // WEIGHT MODULE INITIALIZATION
    weight_module_initialization(SENSOR_PIN_WEIGHT_FIRST_PIN, SENSOR_PIN_WEIGHT_SECOND_PIN);
  #endif

  #if IS_VERBOSE
    // SERIAL INITIALIZATION
    Serial.begin(9600);
    Serial.println("-| Program at Setup...");
  #endif
}

void loop(){
  // TODO you loop code
  // use the RemoteXY structure for data transfer
  // do not call delay()

  // Remotexy Handler - Do Not Touch It
  RemoteXY_Handler ();

  // Run Scenarion, get weight and water level, send it to app
  app_main();
}


// ==================
//  CUSTOM FUNCTIONS
// ==================
void app_main(){

  double food_level = get_food_level(food_sensor_average);
  double water_level = get_water_level(SENSOR_PIN_WATER, true);

  RemoteXY.food_level = int(food_level);
  RemoteXY.level_graph_var1 = RemoteXY.food_level;

  RemoteXY.water_level = int(water_level);
  RemoteXY.level_graph_var2 = RemoteXY.water_level;

  // Remotexy LED Button to D12 Pin Connection
  digitalWrite(PIN_CONNECTION_TEST_BUTTON, (RemoteXY.connection_test_button==0)?LOW:HIGH);

  #if IS_VERBOSE
    print_serial(water_level, food_level);
  #endif
}

#if IS_VERBOSE
  void print_serial(double water_level, double food_level){
  Serial.print("-| Water Level: ");
  Serial.print(water_level);
  Serial.print(" | Food Level: ");
  Serial.println(food_level);
}
#endif

void weight_module_initialization(int weight_sensor_first_pin, int weight_sensor_second_pin){
  /*
    How to calibrate your load cell
    Call set_scale() with no parameter.
    Call tare() with no parameter.
    Place a known weight on the scale and call get_units(10).
    Divide the result in step 3 to your known weight. You should get about the parameter you need to pass to set_scale().
    Adjust the parameter in step 4 until you get an accurate reading.
  */
  scale.begin(weight_sensor_first_pin, weight_sensor_second_pin);   // Initializes the scaling process.
  // Used pins are A0 and A1.

  scale.set_scale();
  scale.tare();          // Resets the scale to 0.
  #if IS_WEIGHT_CALIBRATIN_ACTIVATED
    Serial.println("Initializing scale calibration.");  // Prints user commands.
    Serial.println("Please remove all weight from scale.");
    Serial.println("Place known weights on scale one by one.");
    Serial.println("Press '" + String(MORE_25_BUTTON) + "' to increase calibration factor by " + String(CALIBRATION_FACTOR_CHANGE));
    Serial.println("Press '" + String(LESS_25_BUTTON) + "' to decrease calibration factor by " + String(CALIBRATION_FACTOR_CHANGE));
    Serial.println("Press '" + String(MORE_250_BUTTON) + "' to increase calibration factor by " + String(CALIBRATION_FACTOR_CHANGE * 10));
    Serial.println("Press '" + String(LESS_250_BUTTON) + "' to decrease calibration factor by " + String(CALIBRATION_FACTOR_CHANGE * 10));
    Serial.println("Press '" + String(TARE_BUTTON) + "' for tare");
    Serial.println("Press '" + String(DEFAULT_BUTTON) + "' for change calibration factor to default");
    Serial.println("Press '" + String(EXIT_BUTTON) + "' for tare and finish the calibration");
    calibrate_weight_sensor();
  #endif
  scale.set_scale(calibration_factor);
  scale.tare();          // Resets the scale to calibration_factor.
}


void calibrate_weight_sensor(){
  #if IS_WEIGHT_CALIBRATIN_ACTIVATED
    scale.set_scale();
    scale.tare();          // Resets the scale to 0.
    // Calibration process starts if there is a serial connection present.
    Serial.println("Weight Sensor Calibration started.");
    while(user_input != EXIT_BUTTON){
      // Reads users keyboard inputs.
      user_input = Serial.read();

      // Increases calibration factor by CALIBRATION_FACTOR_CHANGE if MORE_BUTTON key is pressed.
      if (user_input == MORE_25_BUTTON){
        calibration_factor += CALIBRATION_FACTOR_CHANGE;
        scale.set_scale(calibration_factor); //Adjust to this calibration factor
      }

      // Decreases calibration factor by CALIBRATION_FACTOR_CHANGE if LESS_BUTTON key is pressed.
      else if (user_input == LESS_25_BUTTON){
        calibration_factor -= CALIBRATION_FACTOR_CHANGE;
        scale.set_scale(calibration_factor); //Adjust to this calibration factor
      }

      // Increases calibration factor by CALIBRATION_FACTOR_CHANGE if MORE_BUTTON key is pressed.
      if (user_input == MORE_250_BUTTON){
        calibration_factor += CALIBRATION_FACTOR_CHANGE * 10;
        scale.set_scale(calibration_factor); //Adjust to this calibration factor
      }

      // Decreases calibration factor by CALIBRATION_FACTOR_CHANGE if LESS_BUTTON key is pressed.
      else if (user_input == LESS_250_BUTTON){
        calibration_factor -= CALIBRATION_FACTOR_CHANGE * 10;
        scale.set_scale(calibration_factor); //Adjust to this calibration factor
      }

      // Reset the scale to zero if TARE_BUTTON key is pressed.
      else if (user_input == TARE_BUTTON){
        scale.set_scale(calibration_factor); //Adjust to this calibration factor
        scale.tare();
      }

      // Reset the calibration factor to zero if ZERO_BUTTON key is pressed.
      else if (user_input == DEFAULT_BUTTON){
        calibration_factor = default_calibration_factor;
        scale.set_scale(calibration_factor); //Adjust to this calibration factor
      }

      g_food_level = get_raw_food_level(5);

      Serial.println("Calibration Factor:\t" + String(calibration_factor));
      Serial.println("Weight:\t\t" + String(g_food_level));
      delay(700);
    }
  #else
    scale.set_scale(calibration_factor); //Adjust to this calibration factor
    scale.tare();
  #endif
}


double get_water_level(int water_sensor_pin, bool is_persentage){

  double water_level = 0;

  #if IS_SENSOR_SIMULATON_ACTIVE
    water_level = double(sensor_simulater(RANDOM_MIN, RANDOM_MAX));
    if(!is_persentage){
      water_level = water_level / 100.0;
    }
  #else
    // get_raw_water_level will return 0 to 650 range because of sensor output
    water_level = double(get_raw_water_level(water_sensor_pin));

    // We limit our range 0 to 650
    water_level = double(constrain(water_level, 0, 650));
    water_level = double(map(water_level, 0, 650, 0, 100));

    if(!is_persentage){
      water_level = water_level / 100.0;
    }
  #endif

  return water_level;

}


int get_raw_water_level(int water_sensor_pin){
  return analogRead(water_sensor_pin);  // Incoming analog signal read and appointed sensor
}


double get_food_level(int average){

  double raw_weight = 0;

  #if IS_SENSOR_SIMULATON_ACTIVE
    raw_weight = double(sensor_simulater(RANDOM_MIN, RANDOM_MAX));
  #else
    // get_raw_food_level will return 0 to -100 range, empt is 0, 1kg is -100
    raw_weight = get_raw_food_level(average);

    // Negative will be possitive
    raw_weight = -raw_weight;

    // We limit our range with 0 to 100
    raw_weight = double(constrain(int(raw_weight), 0, 100));
  #endif

  return raw_weight;
}


double get_raw_food_level(int average){
  return double(scale.get_units(average));  // Incoming analog signal read and appointed sensor
}


#if IS_SENSOR_SIMULATON_ACTIVE
  long sensor_simulater(int min, int max){
    randomSeed( millis() / 3 );
    return random(min, max);
  }

  long get_passed_time(long option){
    return millis() / option;
  }
#endif
